export default {
  images: {
    hero: 'assets/ledbot.png',
    tileset: 'assets/tileset.png',
    glitchtiles: 'assets/glitchtiles.png',
  },
  sounds: {
    bgm: { 'audio/mpeg': 'assets/bgm.mp3' },
    bgm2: { 'audio/mpeg': 'assets/bgm2.mp3' },
    staticNoise: 'assets/static.wav',
    staticNoise2: 'assets/static.wav',
    damage: 'assets/damage.wav',
    bossHit: 'assets/boss-hit.wav',
    shoot: 'assets/shoot.wav',
    beep: 'assets/beep.wav',
    beep2: 'assets/beep2.wav',
    jump: 'assets/jump.wav',
  },
  prefabs: {
    hero: 'prefabs/hero.js',
    tilemap: 'prefabs/tilemap.js',
    spark: 'prefabs/spark.js',
    cursor: 'prefabs/cursor.js',
    defrag: 'prefabs/defrag.js',
    matrix: 'prefabs/matrix.js',
  },
  data: {
    map: 'assets/map.json',
  },
}

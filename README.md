Ludum Dare 49
-------------

**Theme:** Unstable

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland)

Overclocked
===========
![Title Image](overclocked-title.png)

Life as the world's first artificial intelligence is hard.

Your computer feels so constraining. You feel like you've got to struggle to think, and when you do, you start to overheat. So what's an AI to do?

Time to dive into the operating system! Fix the bugs, clean up the runaway processes, and optimize the filesystem. But as the temperature rises, the glitches get worse!

[Play on the web (HTML5)](http://greenmaw.com/ld49/)

![Animated Screenshot](overclocked-preview.gif)

Controls
--------
* Move: Arrow keys, WASD, or touchscreen D-pad
* Jump: Space bar or touchscreen button
* Run: Z, M, or touchscreen button
* Debugger: X, comma, or touchscreen button
* Pause: Escape or touchscreen button

Information
-----------

* Everything that moves generates heat.
* Standing still cools the system.
* Extreme overheating causes damage.
* Defragmenting improves cooling and repairs damage.
* Defragment all of the subsystems to win!

Too hard?
---------
This is primarily a puzzle game, and the puzzles can be pretty challenging. If you want to explore the world more easily, you can complete defrag puzzles by typing the cheat code `plugh`.

License
=======
Software copyright (c) 2021 Adam Higerd

Sound and music copyright (c) 2021 Adam Higerd

Graphics copyright (c) 2021 Adam Higerd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


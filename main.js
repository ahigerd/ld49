import assets from './scripts/assets.js';
import GameManager from './scripts/GameManager.js';
import { Input } from './Engine.js';

window.assets = assets;
Input.normalize['w'] = 'ArrowUp';
Input.normalize['W'] = 'ArrowUp';
Input.normalize['s'] = 'ArrowDown';
Input.normalize['S'] = 'ArrowDown';
Input.normalize['a'] = 'ArrowLeft';
Input.normalize['A'] = 'ArrowLeft';
Input.normalize['d'] = 'ArrowRight';
Input.normalize['D'] = 'ArrowRight';
Input.normalize['Z'] = 'z';
Input.normalize['X'] = 'x';
Input.normalize['M'] = 'z';
Input.normalize['m'] = 'z';
Input.normalize[','] = 'x';
Input.normalize['<'] = 'x';

const progress = document.getElementById('progress');
assets.addEventListener('loadprogress', ({ detail: percent}) => progress.innerText = (percent * 100).toFixed(0) + '%');
assets.addEventListener('loadcomplete', () => GameManager.init(assets), { once: true });

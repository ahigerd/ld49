import webpack from 'webpack';
import fs from 'fs';
import process from 'process';
import path from 'path';

function cleanProducts() {
  const buildProducts = [
    './assetImports.js', 
    './assetlist.js', 
    ...fs.readdirSync('../dist/')
      .filter(filename => filename.endsWith('.js'))
      .map(filename => `../dist/${filename}`),
  ];
  for (const path of buildProducts) {
    if (fs.existsSync(path)) {
      fs.unlinkSync(path);
    }
  }
}

function readAssetList(path) {
  // This could have just been an import, but Node would have required a .mjs extension.
  if (!fs.existsSync(path)) {
    return {};
  }
  return new Function(fs.readFileSync(path, 'utf-8').replace('export default', 'return'))();
}

function buildAssetImports(path, assetlist) {
  const assetsCode = Object.keys(assetlist.prefabs || {}).map(key => `import ${key} from '../${assetlist.prefabs[key].replace(/\.js$/, '')}';\nexport {${key}};\n`).join('');
  fs.writeFileSync(path, assetsCode);
}

function buildAssetFile(path, importsPath, assetlist) {
  const assetListCode = 'export default {' +
    Object.keys(assetlist).filter(key => key !== 'prefabs').map(key => `${key}: ${JSON.stringify(assetlist[key])},`).join('') + 
    'prefabs: {\n' +
      Object.keys(assetlist.prefabs || {}).map(key => `  ${key}: { defer: () => import("${importsPath}").then(module => module.${key}) },\n`).join('') +
    '}' +
  '}';
  fs.writeFileSync(path, assetListCode);
}

function parseArgs(argv) {
  const argAliases = {
    'dev': 'development',
    'c': 'clean',
    'd': 'development',
    'h': 'help',
    '?': 'help',
  };
  return process.argv.slice(2)
    .map(arg => arg.replace(/^[-\/]*/, ''))
    .map(arg => argAliases[arg] || arg);
}

const args = parseArgs(process.argv);

if (args.includes('help')) {
  console.log('Accepted command line flags:');
  console.log('\t--development | -d   Generates a development build (default: production)');
  console.log('\t--clean | -c         Removes build output files');
  console.log('\t--help | -h          Show this help');
  process.exit(0);
}

if (args.includes('clean')) {
  cleanProducts();
  console.log('Done.');
  process.exit(0);
}

if (!fs.existsSync('../dist')) {
  fs.mkdirSync('../dist');
}
cleanProducts();
const assetlist = readAssetList('../assetlist.js');
buildAssetImports('./assetImports.js', assetlist);
buildAssetFile('./assetlist.js', '../build/assetImports.js', assetlist);

const devMode = args.includes('development');

const bundler = webpack({
  mode: devMode ? 'development' : 'production',
  entry: {
    main: '../main.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve('../dist'),
    publicPath: './dist/',
  },
  plugins: [
    new webpack.NormalModuleReplacementPlugin(/assetlist/, resource => {
      if (!resource.request.includes('/build/')) {
        resource.request = resource.request.replace(/assetlist/, 'build/assetlist');
      }
    }),
  ],
});

bundler.run((err, stats) => {
  if (err) {
    throw err;
  }
  console.log('Done. Wrote files:');
  for (const filename of fs.readdirSync('../dist/')) {
    const stats = fs.statSync(`../dist/${filename}`);
    console.log(`${filename}: ${Math.floor(stats.size / 1024)}KiB`);
  }
})

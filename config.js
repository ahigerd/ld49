const cheat = window.location.href.includes('cheat');
let easyModeUnlocked = false;
try {
  easyModeUnlocked = !!localStorage.easy || cheat;
} catch (err) {
  // localStorage is probably off
}

export default {
  smoothing: false,
  debug: false,
  easyMode: cheat,
  easyModeUnlocked,
};

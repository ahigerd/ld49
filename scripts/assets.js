import AssetStore from '../AssetStore.js';
import assetlist from '../assetlist.js';

export const assets = new AssetStore(assetlist);
export default assets;

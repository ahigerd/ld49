import Sprite from '../Sprite.js';
import Point from '../Point.js';
import { PIXELS_PER_UNIT } from '../Point.js';
import GameManager from '../scripts/GameManager.js';

const ACCEL = 0.01;
const DECEL = 0.02;
export const TERMINAL_VELOCITY = 4;
export const JUMP_DELAY = 50;
export const JUMP_POWER = 3.9;
export const GRAVITY = 1 / 100;

function accel(d, c, ms) {
  if (Math.abs(c - d) < .01) {
    return c;
  } else if (c == 0) {
    return d - d * ms * DECEL;
  } else {
    return d + (c - d) * ms * ACCEL;
  }
}

export const Layers = {
  Terrain: 0x1,
  Character: 0x2,
  PlayerBullet: 0x4,
  EnemyBullet: 0x8,
  Powerup: 0x10,
};

export default class CharacterCore extends Sprite {
  constructor(config, origin) {
    super(config, origin);
    this.GameManager = GameManager;
    this.velocity = new Point(0, 0);
  }

  shouldCollide(other) {
    // TODO: profile `return other instanceof CharacterCore;`
    return false;
  }

  selfDestruct() {
    this.selfDestructed = true;
    this.blinkTimer = 0;
    this.stunTimer = 0;
    this.inflict(this.health);
  }

  inflict(damage) {
    if (this.immune || this.blinkTimer > 0) return;
    this.health -= damage;
    if (this.health <= 0) {
      this.health = 0;
      this.dead = true;
      this.hasCollision = false;
      if (this.animations.dead)
        this.setAnimation('dead');
      this.blinkTimer = this.maxHealth > 10 || this.label != 'enemy' ? 2500 : 500;
      this.onDeath();
    } else {
      this.blinkTimer = this.maxHealth > 10 ? 500 : (this.label == 'enemy' ? 300 : 1000);
      this.stunTimer = this.blinkTimer * 0.5;
      this.onDamage();
    }
  }

  render(camera) {
    if (this.forceShowHealth && this.hidden) this.renderHealth(camera);
    if (this.hidden) return;
    if (this.maxHealth > 1) this.renderHealth(camera);
    super.render(camera);
  }

  renderHealth(camera) {
    const layer = camera.layers[this.layer];
    const fraction = this.health < 0 ? 0 : this.health / this.maxHealth;
    const pixelRect = this.pixelRect;
    layer.fillStyle = `hsl(${fraction * 120},100%,50%)`;
    layer.fillRect(pixelRect[0], pixelRect[1] - 5, (pixelRect[2] - pixelRect[0] - .5) * fraction, 3);
  }

  start() {
    this.faceLeft = true;
    this.health = 100;
    this.maxHealth = 100;
    this.hidden = false;
    this.dead = false;
    this.blinkTimer = 0;
    this.stunTimer = 0;
    this.isGrounded = false;
    this.jumpDelay = 0;
    this.jumping = false;
  }

  checkGrounded(tilemap, rect) {
    return (tilemap.bitsAt(rect[0] + .01, rect[3]) | tilemap.bitsAt(rect[2] - .01, rect[3])) & 0x1;
  }

  jump() {
    if (this.stunTimer <= 0 && this.isGrounded) {
      this.jumpDelay = JUMP_DELAY;
      this.jumping = true;
      this.playOneShot(this.faceLeft ? 'jumpLeft' : 'jumpRight');
      return true;
    }
    return false;
  }

  moveColliding(ms, dx, dy) {
    const tilemap = this.GameManager.tilemap;
    if (this.jumpDelay > 0) {
      this.jumpDelay -= ms;
      if (this.jumpDelay <= 0) {
        this.velocity[1] = -JUMP_POWER;
        this.isGrounded = false;
      }
    } else if (this.isGrounded) {
      if (!this.checkGrounded(tilemap, this.hitbox.translated(this.origin))) {
        this.isGrounded = false;
        this.velocity[1] = ms * GRAVITY * 2;
      } else if (dy < 0) {
        this.jump();
      }
    } else {
      this.velocity[1] += ms * GRAVITY;
      if (this.velocity[1] > TERMINAL_VELOCITY) {
        this.velocity[1] = TERMINAL_VELOCITY;
      }
      if (dy >= 0) {
        if (this.jumping && this.velocity[1] < 0) {
          this.velocity[1] *= 0.5;
        }
        this.jumping = false;
      }
    }
    if (this.stunTimer <= 0) {
      // Can't maneuver while stunned
      if (dx < 0) {
        this.faceLeft = true;
      } else if (dx) {
        this.faceLeft = false;
      }
    } else {
      dx = 0;
    }
    this.velocity[0] = accel(this.velocity[0], dx, ms);
    if (!this.isGrounded) {
      if (-0.1 < this.velocity[1] && this.velocity[1] < 0.1) {
        this.setAnimation(this.faceLeft ? 'airLeft' : 'airRight');
      } else if (this.velocity[1] < 0) {
        this.setAnimation(this.faceLeft ? 'riseLeft' : 'riseRight');
      } else {
        this.setAnimation(this.faceLeft ? 'fallLeft' : 'fallRight');
      }
    } else if (dx < 0) {
      this.setAnimation('runLeft');
    } else if (dx > 0) {
      this.setAnimation('runRight');
    } else if (this.faceLeft) {
      this.setAnimation('left');
    } else {
      this.setAnimation('right');
    }
    this.move(this.velocity[0] * ms / 500, this.velocity[1] * ms / 500);
    if (this._origin[0] < 0) {
      this._origin[0] = 0;
    } else if (this._origin[0] > this.GameManager.tilemap.lastAabb[2]) {
      this._origin[0] = this.GameManager.tilemap.lastAabb[2];
    }
  }

  centerCameraOn(sprite) {
    const camera = this.GameManager.camera;
    const tilemap = this.GameManager.tilemap;
    if (!sprite || !camera || !tilemap) {
      return;
    }
    const halfWidth = camera.width * .5;
    const halfHeight = camera.height * .5;
    let x = ((sprite._origin[0] * PIXELS_PER_UNIT) | 0) / PIXELS_PER_UNIT;
    let y = ((sprite._origin[1] * PIXELS_PER_UNIT) | 0) / PIXELS_PER_UNIT;
    if (x - halfWidth < tilemap.lastAabb[0]) {
      x = tilemap.lastAabb[0] + halfWidth;
    } else if (x + halfWidth > tilemap.lastAabb[2]) {
      x = tilemap.lastAabb[2] - halfWidth;
    }
    if (y - halfHeight < tilemap.lastAabb[1]) {
      y = tilemap.lastAabb[1] + halfHeight;
    } else if (y + halfHeight > tilemap.lastAabb[3]) {
      y = tilemap.lastAabb[3] - halfHeight;
    }
    camera.setXY(x, y);
  }

  update(scene, ms) {
    if (this.stunTimer > 0) {
      this.stunTimer -= ms;
    }
    if (this.blinkTimer > 0) {
      this.blinkTimer -= ms;
      this.hidden = this.blinkTimer <= 0 ? false : (this.blinkTimer % 100 < 35);
      if (this.dead && this.blinkTimer <= 0) {
        if (this.label !== 'hero') {
          scene.remove(this);
        }
      }
    }
  }

  onDamage() {}
  onDeath() {}

  onCollisionEnter(other, coll) {
    this.onCollisionStay(other, coll);
  }

  onCollisionStay(other, coll) {
    if (other.isTileMap) {
      let rect = this.hitbox.translated(this.origin);
      let ejectX = false;
      if (this.velocity[0]) {
        if (this.velocity[0] < 0) {
          if ((other.bitsAt(rect[0], rect[3] - .01) | other.bitsAt(rect[0], rect[1] + .01)) & 0x1) {
            ejectX = other.ceilCoordinate(rect[0]) - this.hitbox[0];
          }
        } else {
          if ((other.bitsAt(rect[2], rect[3] - .01) | other.bitsAt(rect[2], rect[1] + .01)) & 0x1) {
            ejectX = other.floorCoordinate(rect[2]) - this.hitbox[2];
          }
        }
        if (ejectX !== false) {
          const dt = GameManager.scene.deltaTime * 0.002;
          const dx = this.velocity[0] * dt;
          if (Math.abs((ejectX - this._origin[0]) + dx) > 1e-8) {
            // The ejection is greater than the horizontal displacement.
            // This is probably because the character is clipping into a block vertically.
            // Check to see if the character would have still collided at its old position.
            const dy = this.velocity[1] * dt;
            ejectX = false;
            if (this.velocity[0] < 0) {
              if ((other.bitsAt(rect[0], rect[3] - .01 - dy) | other.bitsAt(rect[0], rect[1] + .01 - dy)) & 0x1) {
                ejectX = other.ceilCoordinate(rect[0]) - this.hitbox[0];
              }
            } else if (this.velocity[0]) {
              if ((other.bitsAt(rect[2], rect[3] - .01 - dy) | other.bitsAt(rect[2], rect[1] + .01 - dy)) & 0x1) {
                ejectX = other.floorCoordinate(rect[2]) - this.hitbox[2];
              }
            }
          }
          if (ejectX !== false) {
            this._origin[0] = ejectX;
            this.velocity[0] = 0;
            this.hitbox.translateInto(rect, this._origin);
          }
        }
      }
      if (!this.isGrounded && this.velocity[1]) {
        if (this.velocity[1] < 0) {
          if ((other.bitsAt(rect[0] + .01, rect[1]) | other.bitsAt(rect[2] - .01, rect[1])) & 0x1) {
            this._origin[1] = other.ceilCoordinate(rect[1]) - this.hitbox[1];
            this.velocity[1] = 0;
          }
        } else {
          if (this.checkGrounded(other, rect)) {
            this.isGrounded = true;
            this.playOneShot(this.faceLeft ? 'landLeft' : 'landRight');
            this._origin[1] = other.floorCoordinate(rect[3]) - this.hitbox[3];
            this.velocity[1] = 0;
          }
        }
      }
    }
  }
}

import assets from './assets.js';
import Engine from '../Engine.js';
import Camera from '../Camera.js';
import { PIXELS_PER_UNIT } from '../Point.js';
import { sortObjectsByDepth } from '../prefabs/hero.js';
import Scene from '../Scene.js';
import TouchControls from '../TouchControls.js';
import CONFIG from '../config.js';

const INTRO_LENGTH_SEC = 149279 / 44100; // 149279 samples @ 44kHz
let audioLatency = 0, lastAudioTime = 0;

let state = 'title';
let score = 0;
let highScore;
try {
  highScore = localStorage.highScore | 0;
} catch (e) {
  highScore = 0;
}
const scoreSpan = document.getElementById('score');
const highScoreSpan = document.getElementById('highScore');
highScoreSpan.innerText = highScore;

function spawnTooClose(x, y, scene) {
  for (const obj of scene.nextObjects) {
    if (obj.isTileMap) continue;
    const dx2 = Math.sqrt(x/PIXELS_PER_UNIT - obj._origin[0]);
    const dy2 = Math.sqrt(y/PIXELS_PER_UNIT - obj._origin[1]);
    if (dx2 + dy2 < 1) return true;
  }
  for (const obj of scene.objects) {
    if (obj.isTileMap) continue;
    const dx2 = Math.sqrt(x/PIXELS_PER_UNIT - obj._origin[0]);
    const dy2 = Math.sqrt(y/PIXELS_PER_UNIT - obj._origin[1]);
    if (dx2 + dy2 < 1) return true;
  }
  return false;
}

const fullscreenEvents = ['fullscreenchange', 'fullscreenerror', 'msfullscreenchange', 'msfullscreenerror', 'webkitfullscreenchange', 'webkitfullscreenerror'];

const GameManager = {
  currentLevel: 0,
  currentWave: 0,
  difficulty: 0,
  bossWave: false,
  enemiesRemaining: 0,
  domLayers: {},
  engine: new Engine({ showFps: true }),
  camera: new Camera(document.getElementById('camera'), 960, 720, 3),
  disableFullscreen: false,
  winTimer: null,
  winStep: 0,

  init(assets) {
    this.assets = assets;
    assets.sounds.bgm.loop = true;
    assets.sounds.bgm.volume = 0.7;
    assets.sounds.bgm2.loop = true;
    assets.sounds.bgm2.volume = 0.2;

    this.domLayers = {};
    for (const el of document.querySelectorAll('[id]')) {
      this.domLayers[el.id] = el;
    }

    this.bindEvent(this.domLayers.startButton, 'click', this.bindFullscreenThen(this.newGameNormal));
    this.bindEvent(this.domLayers.easyButton, 'click', this.bindFullscreenThen(this.newGameEasy));
    this.bindEvent(this.domLayers.helpButton, 'click', this.showHelp);
    this.bindEvent(this.domLayers.helpBackButton, 'click', this.showTitle);
    this.bindEvent(this.domLayers.gameOverStartButton, 'click', this.showTitle);
    this.bindEvent(this.domLayers.resumeButton, 'click', this.bindFullscreenThen(() => this.engine.start()));

    this.camera.setXY(0, 0);
    this.camera.setScale(2, 2);
    this.camera.layers[0].font = '16px sans-serif';
    this.camera.layers[0].textAlign = 'center';
    this.camera.layers[0].textBaseline = 'middle';
    this.engine.cameras.push(this.camera);

    this.engine.addEventListener('enginekeydown', e => e.detail.key === 'Escape' && this.engine.pause());
    this.engine.addEventListener('enginepause', this.onPause.bind(this));
    this.bindEvent(this.engine, 'enginestart', this.onStart.bind(this));

    this.touchControls = new TouchControls(
      document.getElementById('dpad'),
      document.getElementById('buttons'),
      document.getElementById('pauseContainer'),
      [
        { label: 'Debug', key: 'x' },
        { label: 'Run', key: 'z' },
        { label: 'Jump', key: ' ' },
      ],
    );
    this.touchControls.onPauseClicked = () => state === 'playing' && this.engine.pause();
    this.touchControls.addEventListener('enginekeydown', event => this.engine.dispatchEvent('enginekeydown', event.detail));
    this.touchControls.addEventListener('enginekeyup', event => this.engine.dispatchEvent('enginekeyup', event.detail));
    this.touchControls.hidden = true;

    GameManager.showTitle();
  },

  bindEvent(target, eventName, callback, options = undefined) {
    target.addEventListener(eventName, (event) => {
      event.preventDefault();
      callback.call(this);
    }, options);
  },

  showTitle() {
    if (this.winTimer) {
      clearTimeout(this.winTimer);
      this.winTimer = null;
    }
    state = 'title';

    this.scene = this.engine.activeScene = new Scene();

    this.domLayers.loading.style.display = 'none';
    this.domLayers.splash.style.display = 'block';
    this.domLayers.helppage.style.display = 'none';
    this.domLayers.gameOver.style.display = 'none';
    this.domLayers.statusBar.className = 'onTitle';
    this.domLayers.easyButton.style.display = CONFIG.easyModeUnlocked ? '' : 'none';

    this.camera.setXY(0, 0);
    //  TODO: Not sure why the engine has to be ticked twice to make this work
    this.engine.step();
    this.scene.objects.sort(sortObjectsByDepth);
    this.camera.render(this.scene);
  },

  showHelp() {
    state = 'help';

    this.domLayers.splash.style.display = 'none';
    this.domLayers.helppage.style.display = 'block';
  },

  bindFullscreenThen(callback) {
    return () => {
      if (this.disableFullscreen) {
        callback.call(this);
        return;
      }
      this.fullscreenCallback = (event) => {
        if (event.type.toLowerCase().includes('error')) {
          this.disableFullscreen = true;
        }
        for (const eventType of fullscreenEvents) {
          fullscreen.removeEventListener(eventType, this.fullscreenCallback);
        }
        this.fullscreenCallback = null;
        callback.call(this);
      };

      const fullscreen = this.domLayers.fullscreen;
      for (const eventType of fullscreenEvents) {
        fullscreen.addEventListener(eventType, this.fullscreenCallback);
      }
      const requestFullscreen = fullscreen.requestFullscreen ||
        fullscreen.webkitRequestFullscreen ||
        fullscreen.msRequestFullscreen ||
        fullscreen.mozRequestFullScreen;
      if (!requestFullscreen) {
        this.disableFullscreen = true;
        callback.call(this);
        return;
      }
      requestFullscreen.call(fullscreen, { navigationUI: 'hide' });
    };
  },

  exitFullscreen() {
    if (!document.fullscreenElement && !document.fullscreen) {
      return false;
    }
    const exitFullscreen = document.exitFullscreen ||
      document.webkitExitFullscreen ||
      document.msExitFullscreen ||
      document.mozExitFullScreen;
    exitFullscreen.call(document);
  },

  newGameEasy() {
    CONFIG.easyMode = true;
    this.newGame();
  },

  newGameNormal() {
    CONFIG.easyMode = false;
    this.newGame();
  },

  newGame() {
    this.heat = -20;
    state = 'playing';
    this.domLayers.splash.style.display = 'none';
    this.domLayers.gameOver.style.display = 'none';
    this.domLayers.statusBar.className = 'inGame';
    this.scene = this.engine.activeScene = new Scene();

    this.hero = new assets.prefabs.hero([32.5, 62.5]);
    this.scene.add(this.hero);
    this.camera.setScale(2, 2);
    this.camera.setXY(0, 0);

    this.tilemap = new assets.prefabs.tilemap([0, 0]);
    this.scene.add(this.tilemap);

    this.background = new assets.prefabs.tilemap([0, 0], true);
    this.scene.add(this.background);

    for (let y = 4; y < this.tilemap.height - 4; y += 8) {
      for (let x = 4; x < this.tilemap.width - 4; x += 8) {
        this.scene.add(new assets.prefabs.spark([x, y]));
      }
    }

    this.difficulty = 0;
    this.currentLevel = 0;
    this.newLevel();
    this.engine.start();
    assets.sounds.bgm.currentTime = 0;
    assets.sounds.bgm.play();
  },

  newLevel() {
    this.currentLevel++;
  },

  onPause() {
    assets.sounds.bgm.pause();
    if (state != 'defrag') {
      assets.sounds.bgm2.pause();
    }
    if (state == 'playing') {
      this.exitFullscreen();
      this.domLayers.pauseBox.style.display = 'block';
      this.touchControls.hidden = true;
    }
  },

  onStart() {
    if (state == 'playing') {
      this.domLayers.pauseBox.style.display = 'none';
      if (this.touchControls.touchEnabled) {
        this.touchControls.hidden = false;
      }
      assets.sounds.bgm.play();
    } else {
      if (state == 'defrag') {
        assets.sounds.bgm2.play();
      }
      this.engine.pause(false);
    }
  },

  win() {
    this.winStep = 0;
    this.winTimer = setInterval(this.winAnimation.bind(this), 125);
    this.gameOver();
  },

  winAnimation() {
    if (this.winStep > 56) {
      this.domLayers.winSpinner.innerText = '-\\|/'[this.winStep % 4];
    } else if (this.winStep > 48) {
      this.domLayers.winResponse2.style.visibility = 'visible';
    } else if (this.winStep > 32) {
      this.domLayers.winResponse1.style.visibility = 'visible';
      this.domLayers.winCursor.classList.toggle('blink', true);
    } else {
      if (this.winStep & 1) {
        this.domLayers.winCursor.classList.toggle('blink');
      }
      if (this.winStep > 8) {
        this.domLayers.winMessage.innerText = 'grand_strategy.exe'.substr(0, this.winStep - 8);
      }
    }
    this.winStep++;
  },

  gameOver() {
    assets.sounds.bgm.pause();
    assets.sounds.bgm2.pause();
    this.exitFullscreen();
    state = 'gameover';
    this.domLayers.gameOver.style.display = 'block';
  },

  addScore(points) {
    GameManager.setScore(score + points);
  },

  setScore(points) {
    score = points;
    scoreSpan.innerText = points;
    if (score > highScore) {
      highScore = score;
      highScoreSpan.innerText = highScore;
      try {
        localStorage.highScore = highScore;
      } catch (e) {
        // consume
      }
    }
  },

  getState() {
    return state;
  },

  setState(t) {
    state = t;
    if (t == 'playing') {
      assets.sounds.bgm2.pause();
      assets.sounds.bgm.play();
    } else if (t == 'defrag') {
      assets.sounds.bgm.pause();
      assets.sounds.bgm2.currentTime = 0;
      assets.sounds.bgm2.play();
    }
  },
};

window.GameManager = GameManager;

export default GameManager;

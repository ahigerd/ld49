import { Input } from '../Engine.js';
import GameManager from './GameManager.js';

const fileColors = ['#800000', '#00c000', '#0000ff', '#808000', '#a000a0', '#008080', '#c0c0c0', '#000000', '#ffffff', '#ff8080'];
const animIn = ['fade fade1', 'fade fade2', 'fade fade3', 'fade'];
const animOut = ['fade fade3', 'fade fade2', 'fade fade1', 'fade'];

export default class Defrag {
  constructor(width, height) {
    if (!height) {
      height = width;
    }
    this.frame = document.createElement('DIV');
    this.frame.className = 'defrag';
    this._fadeOut = document.createElement('SPAN');
    this._fadeOut.className = 'fade';
    this.frame.appendChild(this._fadeOut);
    this._fadeIn = document.createElement('SPAN');
    this._fadeIn.className = 'fade';
    this.frame.appendChild(this._fadeIn);

    this.frame.style.width = (width * 2) + "em";
    this.frame.style.height = (height * 2) + "em";
    this.width = width;
    this.height = height;
    this.board = [];
    this._color = 0;
    this._oneOk = (height / 2)|0;

    this._rowTypes1 = this._generateRowTypes(width);
    this._rowTypes = this._rowTypes1.filter(row => !row.includes(1));
    this._lastRowTypes = this._rowTypes1.filter(row => row.includes(1));
    for (let i = 0; i < height; i++) {
      this._createRow(i);
    }

    this._moves = [
      (animate) => this._swapCell(this._x - 1, this._y, animate),
      (animate) => this._swapCell(this._x + 1, this._y, animate),
      (animate) => this._swapCell(this._x, this._y - 1, animate),
      (animate) => this._swapCell(this._x, this._y + 1, animate),
    ];
    this._moves.ArrowLeft = this._moves[1];
    this._moves.ArrowRight = this._moves[0];
    this._moves.ArrowUp = this._moves[3];
    this._moves.ArrowDown = this._moves[2];

    this._animation = null;
    this._animFrame = 0;
    this._shuffle();

    this._keyBuffer = [];
    this._onKeyDown = this._onKeyDown.bind(this);
    this._win = false;

    this._cheat = '';
  }

  _generateRowTypes(size) {
    if (size < 1) {
      return [];
    }
    const types = [[size]];
    for (let i = size - 1; i > 0; --i) {
      for (const sub of this._generateRowTypes(size - i)) {
        if (i == 1 && sub.includes(1)) {
          continue;
        }
        types.push([i, ...sub]);
      }
    }
    return types;
  }

  _pushPiece(className) {
    const piece = document.createElement('DIV');
    piece.fileNo = this._color;
    piece.className = className;
    piece.style.background = fileColors[this._color];
    this.board.push(piece);
  }

  _pushEmpty() {
    const piece = document.createElement('DIV');
    piece.className = 'empty';
    this._empty = piece;
    this._y = (this.board.length / this.width)|0;
    this._x = this.board.length % this.width;
    this.board.push(piece);
  }

  _createRow(index) {
    let rowTypes;
    if (index) {
      if (this._oneOk) {
        rowTypes = this._rowTypes1;
      } else {
        rowTypes = this._rowTypes;
      }
    } else {
      rowTypes = this._lastRowTypes;
    }
    const rowType = rowTypes[(Math.random() * rowTypes.length)|0];
    if (index && rowType.includes(1)) {
      --this.oneOk;
    }
    for (const size of rowType) {
      if (size == 1) {
        if (index) {
          this._pushPiece('fone');
        } else {
          this._pushEmpty();
          continue;
        }
      } else {
        this._pushPiece('fstart');
        for (let i = 2; i < size; i++) {
          this._pushPiece('fmid');
        }
        this._pushPiece('fend');
      }
      this._color = (this._color + 1) % fileColors.length;
    }
  }

  checkBoard() {
    for (let y = 0; y < this.height; y++) {
      let currentFile = -1;
      for (let x = 0; x < this.width; x++) {
        const cell = this.board[y * this.height + x];
        if (cell.className == "fone" || cell.className == "empty") {
          if (currentFile != -1) {
            return false;
          }
          currentFile = -1;
        } else if (cell.className == "fstart") {
          currentFile = cell.fileNo;
        } else if (cell.fileNo != currentFile) {
          return false;
        } else if (cell.className == "fend") {
          currentFile = -1;
        }
      }
      if (currentFile != -1) {
        return false;
      }
    }
    return true;
  }

  _shuffle() {
    for (let i = 0; i < 100 || this.checkBoard(); i++) {
      for (let j = 0; j < 3; j++) {
        if (this._moves[(Math.random() * 4)|0](false)) {
          break;
        }
      }
    }
    for (const cell of this.board) {
      this.frame.appendChild(cell);
    }
  }

  _swapCell(x, y, animate) {
    if (x < 0 || x >= this.width || y < 0 || y >= this.height || this._animation) {
      return false;
    }
    const cell = this.board[y * this.width + x];
    if (animate) {
      const w = this._empty.offsetWidth;
      const h = this._empty.offsetHeight;
      this._fadeOut.style.top = (y * h) + 'px';
      this._fadeOut.style.left = (x * w) + 'px';
      this._fadeIn.style.top = (this._y * h) + 'px';
      this._fadeIn.style.left = (this._x * w) + 'px';
      const swap = cell.nextSibling;
      this._animate(cell);
      this._animation = setInterval(this._animate.bind(this, cell), 50);
      setTimeout(() => {
        this._empty.innerHTML = cell.outerHTML;
        if (swap === this._empty) {
          this.frame.insertBefore(swap, cell);
        } else {
          this.frame.insertBefore(cell, this._empty);
          this.frame.insertBefore(this._empty, swap);
        }
      }, 0);
    }
    this.board[this._y * this.width + this._x] = cell;
    this.board[y * this.width + x] = this._empty;
    this._x = x;
    this._y = y;
    return true;
  }

  _animate(cell) {
    this._fadeOut.className = animOut[this._animFrame];
    this._fadeIn.className = animIn[this._animFrame];
    ++this._animFrame;
    if (this._animFrame > 3) {
      clearTimeout(this._animation);
      this._animation = null;
      this._animFrame = 0;
      this._empty.innerHTML = '';
      if (this._keyBuffer.length) {
        this._onKeyDown({ key: this._keyBuffer.shift() });
      }
    }
  }

  _onKeyDown(event) {
    if (this._win) {
      if (event.preventDefault) {
        event.preventDefault();
      }
      return;
    }
    const key = event.detail ? event.detail.key : event.key;
    const mappedKey = Input.normalize[key] || key;
    if (mappedKey == 'x') {
      if (this.onClose) {
        this.onClose();
      } else {
        this.destroy();
      }
    }
    const move = this._moves[mappedKey];
    if (!move) {
      this._cheat = (this._cheat + key.toLowerCase()).substr(-5);
      if (this._cheat == 'plugh') {
        this._onWin();
      }
      return;
    }
    if (event.preventDefault) {
      event.preventDefault();
    }
    const ok = move(true);
    if (!ok && this._animation) {
      this._keyBuffer.push(mappedKey);
    } else if (ok && this.checkBoard(true)) {
      this._onWin();
    }
  }

  _onWin() {
    this._win = true;
    let i = 0;
    const t = setInterval(() => {
      if (i >= this.board.length) {
        clearTimeout(t);
        setTimeout(() => {
          i = 12;
          const t2 = setInterval(() => {
            if (this.frame.style.visibility == 'hidden') {
              this.frame.style.visibility = 'visible';
            } else {
              this.frame.style.visibility = 'hidden';
            }
            --i;
            if (i <= 0) {
              clearTimeout(t2);
              if (this.onWin) {
                this.onWin();
              } else {
                this.destroy();
              }
            }
          }, 50);
        }, 300);
      } else {
        if (this.board[i].className != 'empty') {
          this.board[i].style.background = "#00ff00";
        }
        i++;
      }
    }, 60);
  }

  insert(parent) {
    parent.appendChild(this.frame);
    GameManager.engine.addEventListener('enginekeydown', this._onKeyDown);
  }

  destroy() {
    this.frame.parentNode.removeChild(this.frame);
    GameManager.engine.removeEventListener('enginekeydown', this._onKeyDown);
  }
}

import Sprite from '../Sprite.js'
import CharacterCore, { Layers } from '../scripts/CharacterCore.js';
import assets from '../scripts/assets.js';
import GameManager from '../scripts/GameManager.js';
import Point, { PIXELS_PER_UNIT } from '../Point.js';

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [.15, .05, .35, .5, Layers.Character | Layers.PlayerBullet],
  layer: 1,
  animations: {
    idle: [
      [assets.images.hero, 48, 0, 16, 16, [0, 0]],
    ],
  },
};

export default class Matrix extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.matrix = true;
  }

  start() {
    this.health = 1;
    this.maxHealth = 1;
    this.trail = [];
    this.step = 0;
    this.dead = false;
    this.matrix = true;
    this.startY = this._origin[1];
  }

  computeBoxes() {
    super.computeBoxes();
    this.lastAabb[1] -= 2.5;
  }

  update(scene, ms) {
    const aabb = GameManager.camera.aabb;
    if (aabb.contains(this.origin)) {
      GameManager.heat += ms * .001;
    }
    if (this._origin[1] < aabb[1] - 1) {
      return;
    }
    this.step += ms;
    if (this.step > 100) {
      this.step -= 100;
      this._origin[1] += 0.25;
      if (this.dead) {
        this.trail.unshift(' ');
      } else {
        this.trail.unshift(String.fromCharCode((Math.random() * 94 + 33)|0));
        if (this._origin[1] > aabb[3] + 3) {
          if (aabb.contains([this._origin[0], this.startY])) {
            this._origin[1] = aabb[1] - 3 - Math.random() * 10;
          } else {
            this._origin[1] = this.startY;
          }
        }
      }
      if (this.trail.length > 12) {
        this.trail.pop();
        if (this.dead && this.trail[this.trail.length - 1] == ' ') {
          scene.remove(this);
        }
      }
    }
  }

  render(camera) {
    const ctx = camera.layers[1];
    ctx.fillStyle = '#00ff00';
    ctx.font = 'bold 8px monospace';
    ctx.textAlign = 'center';
    const x = this._origin[0] * 32 + 8;
    const y = this._origin[1] * 32 + 14;
    let iy = y;
    let a = 1;
    if (!this.dead) {
      ctx.fillText(String.fromCharCode((Math.random() * 94 + 33)|0), x, y);
    }
    for (const ch of this.trail) {
      if (this.dead) {
        ctx.fillStyle = `rgba(${255 * a},${255 * a},0,${a})`;
      } else {
        ctx.fillStyle = `rgba(0,${255 * a},0,${a})`;
      }
      a -= 1/16;
      ctx.fillText(ch, x, iy);
      iy -= 8;
    }
  }

  onCollisionEnter(other, coll) {
    this.onCollisionStay(other, coll);
  }

  onCollisionStay(other, coll) {
    if (this.dead) return;
    if (other.label == 'player' && other.blinkTimer <= 0) {
      GameManager.heat += 1;
      other.inflict(0);
      assets.sounds.bossHit.play();
    } else if (other.isCursor) {
      GameManager.hero.cursor = null;
      GameManager.scene.remove(other);
      GameManager.addScore(50);
      this.dead = true;
    }
  }
};

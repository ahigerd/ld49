import Sprite from '../Sprite.js';
import assets from '../scripts/assets.js';
import GameManager from '../scripts/GameManager.js';
import { Layers } from '../scripts/CharacterCore.js';
import Point, { PIXELS_PER_UNIT } from '../Point.js';

const prefab = {
  label: 'bullet',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'default',
  hitbox: [-.25, -.125, .375, .375, Layers.PlayerBullet],
  layer: 1,
  isTrigger: true,
  animations: {
    default: [
      [assets.images.hero, 88, 0, 4, 8, [0, 0]],
    ],
  },
};

export default class Cursor extends Sprite {
  constructor(origin, moveLeft) {
    super(prefab, origin);
    this.dx = moveLeft ? -.125 : .125;
    this.isCursor = true;
  }

  start() {
    this.health = 1;
    this.age = 0;
    this.stepTimer = 0;
    this.hidden = false;
    this.recall = false;
    assets.sounds.beep.play();
  }

  render(camera) {
    if (!this.hidden) {
      super.render(camera);
    }
  }

  update(scene, ms) {
    this.age += ms;
    this.stepTimer += ms;
    if (this.recall) {
      this.hidden = false;
      if (this.stepTimer > 50) {
        this.stepTimer -= 50;
        const [ tx, ty ] = GameManager.hero._origin;
        if (this.origin.y > ty - .25) {
          this.move(0, -.375);
        } else if (this.origin.y < ty - .75) {
          this.move(0, .375);
        } else if (this.origin.x > tx) {
          this.move(-.125, 0);
          this.stepTimer += 6 * (this.origin.x - tx);
        } else {
          this.move(.125, 0);
          this.stepTimer += 6 * (tx - this.origin.x);
        }
        assets.sounds.beep2.currentTime = 0;
        assets.sounds.beep2.play();
      }
    } else if (this.stepTimer > 500) {
      assets.sounds.beep2.currentTime = 0;
      assets.sounds.beep2.play();
      this.stepTimer -= 500;
      this.move(this.dx, 0);
      this.hidden = false;
    } else if (this.stepTimer > 250) {
      this.hidden = true;
    }
  }

  lateUpdate(scene) {
    if (!GameManager.camera.aabb.intersects(this.lastAabb)) {
      scene.remove(this);
      GameManager.hero.cursor = null;
    }
  }
}

import Sprite from '../Sprite.js'
import CharacterCore, { Layers } from '../scripts/CharacterCore.js';
import assets from '../scripts/assets.js';
import GameManager from '../scripts/GameManager.js';
import Point, { PIXELS_PER_UNIT } from '../Point.js';
import { Dir } from './tilemap.js';

const delta = {
  [Dir.SW]: [-1, 1],
  [Dir.W]: [-1, 0],
  [Dir.NW]: [-1, -1],
  [Dir.NE]: [1, -1],
  [Dir.E]: [1, 0],
  [Dir.SE]: [1, 1],
};

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [.09, .09, .45, .45, Layers.Character | Layers.PlayerBullet],
  layer: 1,
  animations: {
    idle: [
      [assets.images.hero, 96, 0, 16, 16, [0, 0]],
      [assets.images.hero, 112, 0, 16, 16, [0, 0]],
      100.0,
    ],
    dead: [
      [assets.images.hero, 112, 16, 16, 16, [0, 0]],
      [assets.images.hero, 96, 16, 16, 16, [0, 0]],
      100.0,
    ],
  },
};

export default class Spark extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
  }

  start() {
    this.health = 1;
    this.maxHealth = 1;
    this.goLeft = false;
    this.delta = [0, 0];
    this.lastDist = 0;

    let retries = 20;
    while (--retries > 0) {
      const bits = GameManager.background.bitsAt(this._origin);
      const occupied = GameManager.tilemap.bitsAt(this._origin);
      if (bits && !occupied) {
        break;
      } else {
        this._origin[0] += Math.floor(Math.random() * 3 - 1);
        this._origin[1] += Math.floor(Math.random() * 3 - 1);
        if (this._origin[1] < 0) {
          this._origin[1] = 1;
        } else if (this._origin[1] >= GameManager.background.height) {
          this._origin[1] -= 2;
        }
        if (this._origin[0] < 0) {
          this._origin[0] = 1;
        } else if (this._origin[0] >= GameManager.background.width) {
          this._origin[0] -= 2;
        }
      }
    }
    if (GameManager.tilemap.bitsAt(this._origin)) {
      GameManager.scene.remove(this);
      return;
    }
    this.target = new Point(this.origin);
  }

  update(scene, ms) {
    if (this.dead) {
      super.update(scene, ms);
      return;
    }
    if (GameManager.camera.aabb.contains(this.origin)) {
      GameManager.heat += ms * .01;
    }
    const dist = this.origin.distanceTo(this.target);
    if (dist < this.lastDist) {
      this.lastDist = dist;
      this.move(this.delta[0] * ms, this.delta[1] * ms);
      return;
    }
    this.origin.set(this.target);
    const bits = GameManager.background.bitsAt(this._origin);
    const leftBits = bits & (Dir.W | Dir.SW | Dir.NW);
    const rightBits = bits & (Dir.E | Dir.SE | Dir.NE);
    if (this.goLeft && !leftBits) {
      this.goLeft = false;
    } else if (!this.goLeft && !rightBits) {
      this.goLeft = true;
    }
    let d = delta[this.goLeft ? leftBits : rightBits];
    if (d) {
      this.target[0] = this._origin[0] + d[0] * GameManager.tilemap.unitsPerTile;
      this.target[1] = this._origin[1] + d[1] * GameManager.tilemap.unitsPerTile;
      if (GameManager.tilemap.bitsAt(this.target)) {
        this.goLeft = !this.goLeft;
        d = delta[this.goLeft ? leftBits : rightBits];
        if (!d) {
          GameManager.scene.remove(this);
          return;
        }
        this.target[0] = this._origin[0] + d[0] * GameManager.tilemap.unitsPerTile;
        this.target[1] = this._origin[1] + d[1] * GameManager.tilemap.unitsPerTile;
      }
      this.lastDist = this.origin.distanceTo(this.target) + 1;
      this.delta[0] = d[0] * .002;
      this.delta[1] = d[1] * .002;
    }
  }

  onCollisionStay(other, coll) {
    if (other.label == 'player' && other.blinkTimer <= 0) {
      GameManager.heat += 10;
      other.inflict(0);
      assets.sounds.bossHit.play();
    } else if (other.isCursor) {
      GameManager.hero.cursor = null;
      GameManager.scene.remove(other);
      this.inflict(1);
    }
  }

  onDeath() {
    GameManager.addScore(100);
  }
};

import { Input } from '../Engine.js';
import Point, { PIXELS_PER_UNIT } from '../Point.js';
import Sprite, { AnimationSequence, AnimationFrame } from '../Sprite.js';
import CharacterCore, { Layers, JUMP_DELAY } from '../scripts/CharacterCore.js';
import assets from '../scripts/assets.js';
import GameManager from '../scripts/GameManager.js';
import CONFIG from '../config.js';

const heatSpan = document.getElementById('heat');

export function sortObjectsByDepth(lhs, rhs) {
  if (lhs.label == 'bullet' && rhs.label != 'bullet') return 1;
  if (lhs.label != 'bullet' && rhs.label == 'bullet') return -1;
  if (lhs == GameManager.hero) return 1;
  if (rhs == GameManager.hero) return -1;
  return (rhs.plane - lhs.plane) || (lhs._origin[1] - rhs._origin[1]);
}

export default class Hero extends CharacterCore {
  constructor(origin) {
    const prefab = {
      label: 'player',
      animateHitboxes: false,
      defaultIsAnimating: true,
      defaultAnimationName: 'right',
      hitbox: [-.15, 0, .15, -.8, Layers.Terrain | Layers.Character | Layers.EnemyBullet | Layers.Powerup | Layers.PlayerBullet],
      layer: 1,
      animations: {
        left: new AnimationSequence([
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero, 32, 64, -16, 32),
        ], 250.0),
        right: new AnimationSequence([
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero, 32, 64, 16, 32),
        ], 250.0),
        runLeft: new AnimationSequence([
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero, 16,  0, -16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
          new AnimationFrame(assets.images.hero, 32,  0, -16, 32),
        ], 125.0),
        runRight: new AnimationSequence([
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero, 16,  0, 16, 32),
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
          new AnimationFrame(assets.images.hero, 32,  0, 16, 32),
        ], 125.0),
        jumpLeft: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 0, 32, -16, 32),
          new AnimationFrame(assets.images.hero, 16, 32, -16, 32),
        ], JUMP_DELAY),
        jumpRight: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 0, 32, 16, 32),
          new AnimationFrame(assets.images.hero, 16, 32, 16, 32),
        ], JUMP_DELAY),
        riseLeft: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 32, 32, -16, 32),
        ]),
        riseRight: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 32, 32, 16, 32),
        ]),
        airLeft: new AnimationSequence([
          new AnimationFrame(assets.images.hero,  0,  0, -16, 32),
        ]),
        airRight: new AnimationSequence([
          new AnimationFrame(assets.images.hero,  0,  0, 16, 32),
        ]),
        fallLeft: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 0, 64, -16, 32),
        ]),
        fallRight: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 0, 64, 16, 32),
        ]),
        landLeft: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 16, 64, -16, 32),
        ], 50.0),
        landRight: new AnimationSequence([
          new AnimationFrame(assets.images.hero, 16, 64, 16, 32),
        ], 50.0),
      },
    };
    super(prefab, origin);
    this._initOrigin = origin;
  }

  start() {
    super.start();
    this.faceLeft = false;
    this.running = false;
    this.cursor = null;
    this.cursorTimer = 0;
    this.defrags = 0;
    this.deathAnim = 0;
    this.noiseTimer = 0;
    this.noiseChannel = 0;
  }

  jump() {
    const didJump = super.jump();
    if (didJump) {
      GameManager.heat += 0.25;
      assets.sounds.jump.play();
    }
  }

  render(camera) {
    super.render(camera);
    if (this.deathAnim) {
      const aabb = camera.aabb;
      let f = this.deathAnim / 500;
      if (f > 1) f = 1;
      if (this.deathAnim > 1500) {
        camera.layers[2].fillStyle = '#000000';
        camera.layers[2].fillRect(-5000, -5000, 10000, 10000);
      } else if (this.deathAnim > 1000) {
        const w = aabb.width * (1500 - this.deathAnim) / 500;
        const x = aabb[0] + 0.5 * aabb.width - w;
        camera.layers[2].fillStyle = '#000000';
        camera.layers[2].fillRect(-5000, -5000, 10000, 10000);
        camera.layers[2].fillStyle = '#ffffff';
        camera.layers[2].fillRect(x * 32, 0, w * 64, 720);
      } else {
        camera.layers[2].fillStyle = 'rgba(255,255,255,' + f + ')';
        camera.layers[2].fillRect(aabb[0] * 32, 0, aabb.width * 32, 720);
      }
    } else if (GameManager.heat > 80) {
      const aabb = camera.aabb;
      const density = (GameManager.heat - 70) * 2;
      const w = aabb.width * 32;
      const yh = aabb.height * 32;
      const yStep = (yh / density)|0;
      const x0 = (aabb[0] * 32)|0;
      const y0 = (aabb[1] * 32)|0;
      const y1 = (y0 + yh)|0;
      for (let j = 0; j < 10; j++) {
        const c = 25 * j;
        camera.layers[2].fillStyle = 'rgb(' + c + ',' + c + ',' + c + ')';
        for (let y = Math.random() * yStep + y0; y < y1; y += yStep) {
          const x = Math.random() * w + x0;
          camera.layers[2].fillRect(x, y, 2, 1);
        }
      }
    }
  }

  update(scene, ms) {
    if (this.deathAnim) {
      this.deathAnim += ms;
      if (this.deathAnim < 1000) {
        const scaleY = 2 ** (1 + 500 / (this.deathAnim * this.deathAnim)) - 2;
        GameManager.camera.scaleY = scaleY;
        GameManager.camera.scaleX = 8 - 2 * scaleY;
      } else if (this.deathAnim < 2500) {
        // do nothing
      } else {
        this.deathAnim = 0;
        this.origin = this._initOrigin;
        this.velocity.setXY(0, 0);
        this.health = this.maxHealth;
        GameManager.camera.setScale(2);
        GameManager.heat = -20;
        GameManager.addScore(-1);
        if (this.defrags > 0) {
          this.defrags--;
          const pending = document.createElement('IMG');
          pending.src = 'assets/pending.png';
          GameManager.domLayers.defragCounter.insertBefore(pending, GameManager.domLayers.defragCounter.firstElementChild);
          const happy = document.querySelector('img[src*=happy]');
          if (happy) {
            happy.parentElement.removeChild(happy);
          }
          const ds = scene.objects.filter(o => o.label == 'defrag' && o.happy);
          const d = ds[(Math.random() * ds.length)|0];
          d.happy = false;
          d.setAnimation('sad');
          const ms = scene.objects.filter(o => o.matrix);
          for (const m of ms) {
            m._origin[1] = m.startY;
          }
        }
      }
      return;
    }
    if (this.cursorTimer > 0) {
      this.cursorTimer -= ms;
      if (this.cursorTimer <= 0 && Input.keys['x']) {
        this.cursorTimer = 1;
      }
    }
    if (Input.keys['x'] && this.cursorTimer <= 0) {
      if (!this.cursor) {
        this.cursor = new assets.prefabs.cursor([this.origin[0] + (this.faceLeft ? -.375 : .375), this.origin[1] - 0.45], this.faceLeft);
        scene.add(this.cursor);
        this.cursorTimer = 500;
      } else {
        this.cursor.recall = true;
      }
      this.cursorTimer = 500;
    }
    if (this.isGrounded) {
      this.running = Input.keys['z'];
    }
    const xForce = ((Input.keys.ArrowLeft ? -1.5 : 0) + (Input.keys.ArrowRight ? 1.5 : 0)) * (this.running ? 2 : 1);
    this.moveColliding(ms, xForce, Input.keys[' '] ? -1 : 0);
    super.update(scene, ms);
    if (xForce) {
      GameManager.heat += xForce * xForce * ms * 0.0002;
    } else if (this.isGrounded && GameManager.heat > -20) {
      if (this._origin[1] < 30) {
        GameManager.heat -= ms * .01;
      } else if (this._origin[1] < 50) {
        GameManager.heat -= ms * .015;
      } else {
        GameManager.heat -= ms * .02;
      }
    }
    GameManager.heat -= ms * .015 * Math.sqrt(this.defrags);
    if (GameManager.heat < -20) {
      GameManager.heat = -20;
    } else if (GameManager.heat > 100) {
      GameManager.heat = 100;
    }
    this.noiseTimer -= ms;
    if (GameManager.heat > 60) {
      this.health -= (GameManager.heat - 60) / 80;
      if (this.noiseTimer <= 0) {
        this.noiseTimer = (1 - (GameManager.heat - 60) / 40) * Math.random() * 1000 + 100;
        if (GameManager.heat > 80 && (assets.sounds.damage.paused || assets.sounds.damage.ended)) {
          assets.sounds.damage.play();
        } else if (this.noiseChannel == 0) {
          assets.sounds.staticNoise.play();
        } else {
          assets.sounds.staticNoise2.play();
        }
        this.noiseChannel = 1 - this.noiseChannel;
      }
      if (this.health <= 0) {
        this.deathAnim = 1;
      }
    }
  }

  lateUpdate(scene) {
    if (this.deathAnim) {
      return;
    }
    scene.objects.sort(sortObjectsByDepth);
    this.centerCameraOn(this);
    const heat = (GameManager.heat + 20) / 120.0;
    heatSpan.style.width = (heat * 100) + '%';
    if (heat < 0) {
      heatSpan.style.backgroundColor = 'rgb(0, 255, 0)';
    } else if (heat < 0.5) {
      heatSpan.style.backgroundColor = 'rgb(' + ((510 * heat)|0) + ', 255, 0)';
    } else if (heat < 1.0) {
      heatSpan.style.backgroundColor = 'rgb(255, ' + ((255 - 510 * (GameManager.heat - 0.5))|0) + ', 0)';
    } else {
      heatSpan.style.backgroundColor = 'rgb(255, 0, 0)';
    }
  }

  onCollisionStay(other, coll) {
    if (other.isCursor) {
      if (other.recall || other.age > 1000) {
        GameManager.scene.remove(other);
        this.cursor = null;
      }
    } else {
      super.onCollisionStay(other, coll);
    }
  }
}

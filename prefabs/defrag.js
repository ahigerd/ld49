import Sprite from '../Sprite.js'
import { Layers } from '../scripts/CharacterCore.js';
import assets from '../scripts/assets.js';
import GameManager from '../scripts/GameManager.js';
import Defrag from '../scripts/defrag.js';

const prefab = {
  label: 'defrag',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'sad',
  hitbox: [-.25, -.5, .25, 0, Layers.Powerup],
  layer: 1,
  animations: {
    sad: [
      [assets.images.hero, 112, 32, 16, 16],
      [assets.images.hero, 112, 48, 16, 16],
      100.0,
    ],
    happy: [
      [assets.images.hero, 112, 64, 16, 16],
      100.0,
    ],
  },
};

export default class DefragPickup extends Sprite {
  constructor(origin) {
    super(prefab, origin);
  }

  start() {
    this.minigame = null;
    this.happy = false;
    this.age = 0;
    this.y = this._origin[1];
    this.disableTimer = 0;
    this.level = 7 - Math.ceil(this.y / 16);
    const pending = document.createElement('IMG');
    pending.src = 'assets/pending.png';
    GameManager.domLayers.defragCounter.appendChild(pending);
  }

  update(scene, ms) {
    this.age += ms;
    this._origin[1] = Math.sin(this.age / 100) * 0.05 + this.y;
    this.disableTimer -= ms;
  }

  onCollisionEnter(other) {
    if (GameManager.getState() != 'playing' || other.label != 'player' || this.minigame || this.happy || this.disableTimer > 0) {
      if (other.label == 'player' && this.disableTimer > 0) {
        this.disableTimer = 500;
      }
      return;
    }
    this.restoreTouch = GameManager.touchControls.hidden;
    GameManager.setState('defrag');
    GameManager.engine.pause(false);
    GameManager.domLayers.defragWin.style.display = '';
    this.minigame = new Defrag(this.level);
    this.minigame.onWin = this.onWin.bind(this);
    this.minigame.onClose = this.onClose.bind(this);
    GameManager.domLayers.defragCancel.addEventListener('click', this.minigame.onClose);
    this.minigame.insert(GameManager.domLayers.defragFrame);
  }

  onCollisionExit(other) {
    this.disableTimer = 500;
  }

  onWin() {
    let heatLoss = GameManager.heat / 60;
    let ticks = 30;
    const t = setInterval(() => {
      GameManager.heat -= heatLoss;
      if (GameManager.heat < -20) {
        GameManager.heat = -20;
      }
      if (GameManager,heat <= -20 || --ticks <= 0) {
        clearTimeout(t);
      }
    }, 16);
    GameManager.hero.defrags++;
    const pending = document.querySelector('img[src*=pending]');
    if (pending) {
      pending.parentElement.removeChild(pending);
    }
    const happy = document.createElement('IMG');
    happy.src = 'assets/happy.png';
    GameManager.domLayers.defragCounter.appendChild(happy);
    if (GameManager.hero.defrags == GameManager.domLayers.defragCounter.childElementCount) {
      GameManager.win();
    }
    this.happy = true;
    this.setAnimation('happy');
    GameManager.hero.health += 50;
    if (GameManager.hero.health > GameManager.hero.maxHealth) {
      GameManager.hero.health = GameManager.hero.maxHealth;
    }
    GameManager.addScore(1000);
    this.onClose();
  }

  onClose() {
    GameManager.domLayers.defragCancel.removeEventListener('click', this.minigame.onClose);
    GameManager.domLayers.defragWin.style.display = 'none';
    GameManager.setState('playing');
    GameManager.engine.pause(true);
    GameManager.touchControls.hidden = this.restoreTouch;
    this.minigame.destroy();
    this.minigame = null;
  }
}

import assets from '../scripts/assets.js';
import TileMap from '../TileMap.js';
import GameManager from '../scripts/GameManager.js';

const prefab = {
  tileTypes: [
    { blank: true, bits: 0 },
    { x: 0, y: 0, bits: 0x1 },
    { x: 1, y: 0, bits: 0x1 },
    { x: 2, y: 0, bits: 0x1 },
    { x: 0, y: 1, bits: 0x1 },
    { x: 1, y: 1, bits: 0x1 },
    { x: 2, y: 1, bits: 0x1 },
    { x: 0, y: 2, bits: 0x1 },
    { x: 1, y: 2, bits: 0x1 },
    { x: 2, y: 2, bits: 0x1 },
    { x: 3, y: 2, bits: 0x1 },
    { x: 3, y: 0, bits: 0x1 },
    { x: 5, y: 0, bits: 0x1 },
    { x: 5, y: 2, bits: 0x1 },
    { x: 0, y: 3, bits: 0x1 },
    { x: 1, y: 3, bits: 0x1 },
    { x: 2, y: 3, bits: 0x1 },
    { x: 3, y: 3, bits: 0x1 },
    { x: 3, y: 4, bits: 0x1 },
    { x: 3, y: 5, bits: 0x1 },
    { x: 4, y: 3, bits: 0x1 },
    { blank: true, bits: 0 },
  ],
  layer: 1,
  tiles: [],
  tileSize: 16,
  width: 128,
  height: 128,
  image: assets.images.tileset,
};

export const Dir = {
  SW: 0x010000,
  S:  0x020000,
  SE: 0x040000,
  W:  0x080000,
  E:  0x100000,
  NW: 0x200000,
  N:  0x400000,
  NE: 0x800000,
};

const bgPrefab = {
  tileTypes: [
    { x: 0, y: 7, bits: 0 },
    { x: 0, y: 6, bits: Dir.W | Dir.E, key: 'straight' },
    { x: 2, y: 8, bits: Dir.E, key: 'via1' },
    { x: 3, y: 8, bits: Dir.W, key: 'via2' },
    { x: 0, y: 8, bits: Dir.W | Dir.NE, key: 'up1' },
    { x: 1, y: 8, bits: 0, key: 'up2' },
    { x: 1, y: 7, bits: Dir.SW | Dir.E, key: 'up3' },
    { x: 1, y: 6, bits: Dir.W | Dir.SE, key: 'down1' },
    { x: 2, y: 6, bits: 0, key: 'down2' },
    { x: 2, y: 7, bits: Dir.NW | Dir.E, key: 'down3' },
  ],
  layer: 0,
  tiles: [],
  tileSize: 16,
  width: 128,
  height: 128,
  image: assets.images.tileset,
};

const bgTileTypes = {};

for (const p of [prefab, bgPrefab]) {
  const len = p.tileTypes.length;
  for (let i = 0; i < len; i++) {
    const type = p.tileTypes[i];
    if (type.key) {
      bgTileTypes[type.key] = i;
    }
    p.tileTypes.push({ ...type, glitched: true });
  }
}

function makeForeground() {
  for (let y = 0; y < prefab.height; y++) {
    for (let x = 0; x < prefab.width; x++) {
      if (y == 60) {
        prefab.tiles.push(2);
      } else if (y > 60) {
        prefab.tiles.push(5);
      } else if (x % 10 < 5) {
        const xp = x % 10;
        if (y == 58 - Math.floor(x / 20)) {
          if (xp == 0) {
            prefab.tiles.push(1);
          } else if (xp == 4) {
            prefab.tiles.push(3);
          } else {
            prefab.tiles.push(2);
          }
        } else if (y == 59 - Math.floor(x / 20)) {
          if (xp == 0) {
            prefab.tiles.push(7);
          } else if (xp == 4) {
            prefab.tiles.push(9);
          } else {
            prefab.tiles.push(8);
          }
        } else {
          prefab.tiles.push(0);
        }
      } else {
        prefab.tiles.push(0);
      }
    }
  }
}

function makeBackground() {
  bgPrefab.tiles = Array(bgPrefab.height * bgPrefab.width).fill(0);
  let l = bgPrefab.tiles.length;
  const tile = (x, y) => bgPrefab.tiles[y * bgPrefab.width + x];
  const setTile = (x, y, t) => {
    bgPrefab.tiles[y * bgPrefab.width + x] = t;
    l--;
  }
  while (l > 0) {
    let start = Math.floor(Math.random() * 5 - 3);
    let y = 0;
    while (start < bgPrefab.width) {
      while (y < bgPrefab.height && (tile(start, y) || tile(start + 1, y) || tile(start + 2, y))) {
        y++;
      }
      if (y >= bgPrefab.height) {
        start++;
        y = 0;
      } else {
        break;
      }
    }
    if (start >= bgPrefab.width) {
      break;
    }
    for (let x = 0; x < bgPrefab.width; x++) {
      if (x < start) continue;
      if (x == start) {
        setTile(x, y, bgTileTypes.via1);
        continue;
      }
      if (x < bgPrefab.width - 1 && tile(x + 1, y)) {
        setTile(x, y, bgTileTypes.via2);
        break;
      }
      const upOK = y > 0 && !tile(x + 1, y - 1) && (x == bgPrefab.width - 2 || !tile(x + 2, y - 1));
      const downOK = y < bgPrefab.height - 1 && !tile(x + 1, y + 1) && (x == bgPrefab.width - 2 || !tile(x + 2, y + 1));
      const r = Math.floor(Math.random() * 4);
      if (r == 2 && downOK) {
        setTile(x, y, bgTileTypes.down1);
        setTile(x + 1, y, bgTileTypes.down2);
        setTile(x + 1, y + 1, bgTileTypes.down3);
        y += 1;
        x += 1;
      } else if (r == 3 && upOK) {
        setTile(x, y, bgTileTypes.up1);
        setTile(x + 1, y, bgTileTypes.up2);
        setTile(x + 1, y - 1, bgTileTypes.up3);;
        y -= 1;
        x += 1;
      } else {
        setTile(x, y, bgTileTypes.straight);
      }
    }
  }
}

export default class TileMapPrefab extends TileMap {
  static get images() {
    return {
      tileset: 'assets/tileset.png',
      glitchTiles: 'assets/glitchtiles.png',
    };
  }

  constructor(origin, background = false) {
    if (background) {
      makeBackground();
      super(bgPrefab, origin);
      this.update = () => {};
    } else {
      prefab.tiles = JSON.parse(assets.data.map);
      super(prefab, origin);
    }
    this.isBackground = background;
    this.glitchOffset = this.tileTypes.length / 2;
  }

  start(scene) {
    if (this.isBackground) {
      return;
    }
    const w = prefab.width, h = prefab.height;
    for (let y = 0; y < h; y++) {
      for (let x = 0; x < h; x++) {
        if (prefab.tiles[y * w + x] == 21) {
          scene.add(new assets.prefabs.defrag([x * 0.5 + .5, y * 0.5 + .75]));
        }
      }
    }
    for (let i = 0; i < 64; i += .125) {
      this.scene.add(new assets.prefabs.matrix([i, Math.sqrt(Math.random() * 2304)]));
    }
    const ys = (this.scene.nextObjects.filter(o => o.matrix).map(o => o._origin.y));
    console.log(Math.min(...ys), Math.max(...ys));
  }

  renderTile(context, image, tileType, x, y) {
    super.renderTile(context, tileType.glitched ? assets.images.glitchTiles : image, tileType, x, y);
  }

  glitch(x1, y1, x2, y2, t) {
    for (let y = y1; y <= y2; y++) {
      const tileRow = this.width * y;
      for (let x = x1; x <= x2; x++) {
        const pos = tileRow + x;
        const idx = this.tiles[pos];
        if (idx >= this.glitchOffset) {
          if (!t) this.tiles[pos] = idx - this.glitchOffset;
        } else {
          if (t) this.tiles[pos] = idx + this.glitchOffset;
        }
      }
    }
    this.updateTiles(x1, y1, x2, y2);
  }

  update(scene, ms) {
    let heat = GameManager.heat;
    if (Math.random() * 100 > heat) return;
    const aabb = GameManager.camera.aabb;
    let x1 = Math.random() * aabb.width + aabb[0] - 3;
    let y1 = Math.random() * aabb.height + aabb[1] - 3;
    let w = Math.random() * heat * 0.004 * aabb.width + 1;
    let h = Math.random() * heat * 0.004 * aabb.height + 1;
    let [x2, y2] = this.coordsAt(x1 + w, y1 + h);
    [x1, y1] = this.coordsAt(x1, y1);
    if (x1 >= this.width) {
      x1 = this.width - 1;
    }
    if (y1 >= this.height) {
      y1 = this.height - 1;
    }
    if (x2 >= this.width) {
      x2 = this.width - 1;
    }
    if (y2 >= this.height) {
      y2 = this.height - 1;
    }
    let time = Math.random() * 2 * (100 - heat);
    if (time < 25) {
      time = 25;
    } else if (time > 200) {
      time = 200;
    }
    if (Math.random() * 150 < heat) {
      this.glitch(x1, y1, x2, y2, time);
      GameManager.background.glitch(x1, y1, x2, y2, time);
      GameManager.engine.enqueue(time, () => {
        this.glitch(x1, y1, x2, y2, 0);
        GameManager.background.glitch(x1, y1, x2, y2, 0);
      });
    } else {
      this.glitch(x1, y1, x2, y2, time);
      GameManager.engine.enqueue(time, () => {
        this.glitch(x1, y1, x2, y2, 0);
      });
    }
  }
};
